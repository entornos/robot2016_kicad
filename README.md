# Instalación

Último  kicad y xsltproc (para BOM) 
   
    add apt-repository ppa:js-reynaud/ppa-kicad
    apt-get update
    apt-get install kicad xsltproc

Versión de kicad actual: 0.201501132116+5369~20~ubuntu14.04.1

Bajar las librerías de Walter Lain de github:

    mkdir ~/walterlain
    git clone git://smisioto.eu/kicad_libs.git

Versión de las librerías:

    commit 133e5a459c062a47eee1bcf457f0e78e4ef0ecc5
    Author: kcswalter <kcswalter@tiscali.it>
    Date:   Sun Sep 28 17:06:56 2014 +0200

        20140928 release

Copiar a /usr/local/kicad

    mkdir /usr/local/kicad
    cd ~/walterlain/kicad_libs
    cp -r library modules /usr/local/kicad

En ~/.bashrc:

    export KIGITHUB='https://github.com/kicad'
    export KISYSMOD=/usr/local/kicad/modules
    export KISYS3DMOD=/usr/local/kicad/modules/packages3d

En ~/.config/kicad:
    cp fp-lib-table fp-lib-table-original
    cp ~/walterlain/kicad_libs/fp-lib-table .

(con esto tenemos las librerias de footprints en la config global)

En eeschema, borrar todas las librerías del sistema y añadir las de walter, que están en /usr/local/kicad/library (seleccionar la ruta en la lista de abajo y luego botón de añadir arriba)

En eeschema, BOM -> Añadir Plugin -> /usr/lib/kicad/plugins/bom2csv.xsl. En command line, modificar "%O" por "%O.csv"

# Autorrutado

En PCBnew, exportar el DSN a freerouting/robot.dsn

Script python que encoge los bordes para simular el clearence cobre-borde (0.3 mm). Además hay que quitar los planos de tierra porque en freerouter hay que desactivar la opción "ignore conduction areas" para que no intente rutar pistas por medio de los disipadores, y si hay planos de tierra no podrá rutar nada por ellos tampoco. El script también elimina los planos de tierra. Al reimportar en Pcbnew el DRC los reactivará y absorberán la red GND.

    python edge_clearance/edge_clearance.py freerouting/robot.dsn 300

Freerouting:

    java -jar freerouting/Freerouting.jar -de freerouting/robot.dsn -di freerouting

En freerouting -> Parameter -> Route:

- Desactivar "Ignore conduction areas" para que no rute pistas por los disipadores
- Desactivar "Restrict pin exit directions" para que puedan salir pistas de los pines en ángulo

Si le cuesta enrutar, bajarle el coste a las vías. En Parameter -> Autoroute -> Details:

- Coste de las vías: 10

Autorrutar y exportar .ses.

Importar el .ses en kicad.

Pasar DRC. Posiblemente el DRC dé errores en las conexiones de las pistas a los disipadores porque el Freerouting apura mucho. Alargar esas pistas hasta que se metan bien dentro del disipador y ya.

# Generación de gerbers

En Pcbnew: File -> Plot:

- Output directory: gerber/
- Layers: F.Cu, B.Cu, B.SilkS, F.SilkS, B.Mask, F.Mask
- Options: Plot footprint references
- Gerber options: Use Protel filename extensions
- Format: 4.6

No usar "subtract soldermask", según el MCN gerber viewer aparentemente al silkscreen se le *suma* el soldermask (con el gerbv y el visor del kicad se ve bien, pero por si acaso).

Generate drill files:

- Units: inches
- Zeros format: Decimal format
- Dill Origin: absolute 

En algunos sitios recomiendan:

- Zeros format: Suppress leading zeros
- Drill file format: Minimal header

(veremos qué pasa).

# Programación de Arduinos a 3.3v con AVRISP de Pololu

*Nota 1*: Teóricamente los arduinos de Banggood ya vienen con el bootloader, así que esto sólo valdría si queremos cambiar el bootloader o los fuses.

*Nota 2*: Posiblmenente alimentando el Arduino con +5V por VCC ya no haga falta modificar el AVRISP como se explica aquí.

Bajar Pololu USB SDK: https://www.pololu.com/docs/0J41

Instalar:

```
sudo apt-get install libusb-1.0-0-dev mono-gmcs mono-devel libmono-winforms2.0-cil
```

Compilar:

```
make
```

En la carpeta PgmCmd:

```
./pgmcmd --list
./pgmcmd --status
```

Saldrá:

```
Target VDD allowed minimum:   4384 mV
```

Cambiamos a 3.2V:

```
./pgmcmd --vddmin 3200
```
