#!/bin/bash

PREFIX=robot

if [ $# -ne 1 ]
then
  echo "$0 dst_dir"
  exit 1
fi

dstdir="$1"
zipfile="$PREFIX-seeed.zip"

if [ ! -d "$dstdir" ]
then
  mkdir "$dstdir"
fi

cp "$PREFIX"*gbl "$dstdir/$PREFIX.gbl"
cp "$PREFIX"*gbs "$dstdir/$PREFIX.gbs"
cp "$PREFIX"*gbo "$dstdir/$PREFIX.gbo"
cp "$PREFIX"*gtl "$dstdir/$PREFIX.gtl"
cp "$PREFIX"*gts "$dstdir/$PREFIX.gts"
cp "$PREFIX"*gto "$dstdir/$PREFIX.gto"
cp "$PREFIX"*drl "$dstdir/$PREFIX.drl"
cp "$PREFIX"*gm1 "$dstdir/$PREFIX.gm1"

if [ -f "$zipfile" ]
then
	rm "$zipfile"
fi

zip -9 -j "$zipfile" "$dstdir"/*
