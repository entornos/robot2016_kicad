EESchema Schematic File Version 2
LIBS:robot-rescue
LIBS:robot
LIBS:conn
LIBS:power
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:w_memory
LIBS:w_microcontrollers
LIBS:w_opto
LIBS:w_relay
LIBS:w_rtx
LIBS:w_transistor
LIBS:w_vacuum
LIBS:robot-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LED LD2
U 1 1 56BAEDC9
P 4850 3550
F 0 "LD2" H 4850 3650 40  0000 C CNN
F 1 "LED_Y" H 4850 3450 40  0000 C CNN
F 2 "w_smd_leds:Led_1206" H 4850 3550 60  0001 C CNN
F 3 "" H 4850 3550 60  0000 C CNN
	1    4850 3550
	1    0    0    -1  
$EndComp
$Comp
L LED LD1
U 1 1 56BAEDD0
P 4850 3200
F 0 "LD1" H 4850 3300 40  0000 C CNN
F 1 "LED_R" H 4850 3100 40  0000 C CNN
F 2 "w_smd_leds:Led_1206" H 4850 3200 60  0001 C CNN
F 3 "" H 4850 3200 60  0000 C CNN
	1    4850 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3550 4800 3550
Connection ~ 4700 3550
Wire Wire Line
	4700 3200 4800 3200
$Comp
L R R18
U 1 1 56BAEDE3
P 5300 3550
F 0 "R18" V 5250 3600 50  0000 C CNN
F 1 "470/680" V 5350 3600 50  0000 C CNN
F 2 "w_smd_resistors:r_1206" H 5300 3550 60  0001 C CNN
F 3 "" H 5300 3550 60  0000 C CNN
	1    5300 3550
	0    -1   -1   0   
$EndComp
$Comp
L R R17
U 1 1 56BAEDEA
P 5300 3200
F 0 "R17" V 5250 3250 50  0000 C CNN
F 1 "470/680" V 5350 3250 50  0000 C CNN
F 2 "w_smd_resistors:r_1206" H 5300 3200 60  0001 C CNN
F 3 "" H 5300 3200 60  0000 C CNN
	1    5300 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 3200 5200 3200
Wire Wire Line
	4950 3550 5200 3550
$Comp
L +3.3VP #PWR056
U 1 1 56BAEDF7
P 4600 3550
F 0 "#PWR056" H 4650 3580 20  0001 C CNN
F 1 "+3.3VP" H 4600 3640 30  0000 C CNN
F 2 "" H 4600 3550 60  0000 C CNN
F 3 "" H 4600 3550 60  0000 C CNN
	1    4600 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 3200 5800 3200
Wire Wire Line
	5400 3550 5800 3550
Text HLabel 5800 3200 2    60   Input ~ 0
RED_LED
Text HLabel 5800 3550 2    60   Input ~ 0
YELLOW_LED
Wire Wire Line
	4700 3200 4700 3550
$EndSCHEMATC
