from clipper import * 
import re

polys = []
poly = []
lines = {}
firstPoint = None

edgeRe = re.compile(r'\(gr_line \(start ([\d\.]+) ([\d\.]+)\) \(end ([\d\.]+) ([\d\.]+)\).*?\(layer Edge.Cuts\) \(width ([\d\.]+)\)')

#edgeRe = re.compile(r'\(gr_line \(start (\d+)')

with open("robot.kicad_pcb") as f:
	for l in f:
		m = edgeRe.search(l)
		if (m is not None):
			print m.group(5)
			[x0, y0, x1, y1, width] = [
				int(float(m.group(1)) * 1000),
				int(float(m.group(2)) * 1000),
				int(float(m.group(3)) * 1000),
				int(float(m.group(4)) * 1000),
				int(float(m.group(5)) * 1000)
			]
			p0 = Point(x0, y0)
			p1 = Point(x1, y1)
			if firstPoint is None:
				firstPoint = p0
			if p0 not in lines:
				lines[p0] = { p1: None }
			else:
				lines[p0][p1] = None
			if p1 not in lines:
				lines[p1] = { p0: None }
			else:
				lines[p1][p0] = None

print "------"
print len(lines)
print len(lines)

point = firstPoint

while (lines):
	print str(type(point)) + " " + str(point)
	poly.append(point)
	nextPoint = lines[point].keys()[0]
	del lines[point][nextPoint]
	if not lines[point]:
		del lines[point]
	del lines[nextPoint][point]
	if not lines[nextPoint]:
		del lines[nextPoint]
	point = nextPoint

print str(type(point)) + " " + str(firstPoint)
poly.append(firstPoint)



poly2 = CleanPolygon(poly)
print "poly  ->" + str(poly)
print "poly2 ->" + str(poly2)

polys.append(poly2)

polys2 = OffsetPolygons(polys, -300, JoinType.Miter)

print polys2[0]
