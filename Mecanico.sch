EESchema Schematic File Version 2
LIBS:robot-rescue
LIBS:robot
LIBS:conn
LIBS:power
LIBS:w_analog
LIBS:w_connectors
LIBS:w_device
LIBS:w_logic
LIBS:w_memory
LIBS:w_microcontrollers
LIBS:w_opto
LIBS:w_relay
LIBS:w_rtx
LIBS:w_transistor
LIBS:w_vacuum
LIBS:robot-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor MOTOR1
U 1 1 56BB1C25
P 4050 2900
F 0 "MOTOR1" H 4050 2850 60  0000 C CNN
F 1 "Motor" H 4050 2950 60  0000 C CNN
F 2 "Robot2015:Pololu_brackets_with_HP_MOTOR" H 4050 2900 60  0001 C CNN
F 3 "" H 4050 2900 60  0000 C CNN
	1    4050 2900
	1    0    0    -1  
$EndComp
$Comp
L Motor MOTOR2
U 1 1 56BB1CB9
P 4050 3200
F 0 "MOTOR2" H 4050 3150 60  0000 C CNN
F 1 "Motor" H 4050 3250 60  0000 C CNN
F 2 "Robot2015:Pololu_brackets_with_HP_MOTOR" H 4050 3200 60  0001 C CNN
F 3 "" H 4050 3200 60  0000 C CNN
	1    4050 3200
	1    0    0    -1  
$EndComp
$Comp
L Rueda_loca RL1
U 1 1 56BB1CD7
P 4050 3500
F 0 "RL1" H 4050 3450 60  0000 C CNN
F 1 "Rueda_loca" H 4050 3550 60  0000 C CNN
F 2 "Robot2015:Pololu_ball_caster" H 4050 3500 60  0001 C CNN
F 3 "" H 4050 3500 60  0000 C CNN
	1    4050 3500
	1    0    0    -1  
$EndComp
$Comp
L Logo LOGO1
U 1 1 56BB20CA
P 4050 3800
F 0 "LOGO1" H 4050 3750 60  0000 C CNN
F 1 "Logo" H 4050 3850 60  0000 C CNN
F 2 "Robot2015:libertas_logo" H 4050 3800 60  0001 C CNN
F 3 "" H 4050 3800 60  0000 C CNN
	1    4050 3800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
