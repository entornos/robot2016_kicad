# from clipper import Area, Clipper, Point, ClipType, PolyType, PolyFillType
from clipper import * 
import re
import shutil
import sys

if len(sys.argv) < 3:
	sys.stderr.write('Usage: clear_edge.py project.dsn micrometers (eg. 300)\n')
	sys.exit(1)

name = sys.argv[1]
delta = int(sys.argv[2])

poly = []
lines = []
boundaryRe = re.compile(r'\(boundary\s+\(path pcb 0(.*?)\)\s*?\)', re.DOTALL|re.MULTILINE)
gndRe = re.compile(r'\(plane GND \(polygon.*?\)\s*?\)', re.MULTILINE|re.DOTALL)

with open(name) as f:
	dsn = "".join(f.readlines())

m = boundaryRe.search(dsn)
numbers = m.group(1).split()

i = 0
while i < len(numbers):
	x = int(numbers[i])
	y = int(numbers[i+1])
	p = Point(x, y)
	poly.append(p)
	i += 2


poly2 = OffsetPolygons([poly], -delta, JoinType.Miter)[0]

newBoundary = "(boundary (path pcb 0 "
for p in poly2:
	newBoundary += str(int(p.x)) + " " + str(int(p.y)) + " "
newBoundary += "))"

dsn = boundaryRe.sub(newBoundary, dsn)
dsn = gndRe.sub('', dsn)

shutil.copyfile(name, name + ".original")
with open(name, "w") as f:
	f.write(dsn)

